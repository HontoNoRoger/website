---
title: AWS RDS Multi-AZ Failover Replication
description: 
date: 2019-12-30T18:04:15.216Z
tags: [AWS, RDS]
---

Wow, good thing that the replication for failover instances for RDS instances (not Aurora) work on an EBS level and not by using the database's replication technique. This came in handy when our read replication was inconsistent and therefore rendered useless due to different settings between the master and the replication instance. Fortunately though, the hidden failover instance wasn't affected due to the different replication technique.
