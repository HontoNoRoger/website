---
title: "Nixie Tube Clock"
description: "A present with a cool retro touch"
date: 2017-10-31T17:41:30.939Z
tags: [DIY]
---

The Nixie clock DIY kit I got as birthday present is finally assembled! Thanks guys!!

![nixie-tube-clock](/images/nixie-tube-clock/assembled.jpg)

![front](/images/nixie-tube-clock/front.jpg)

I later also 3D printed a case for it, which unfortunately
doesn't hold together very well and I have to redo it.
