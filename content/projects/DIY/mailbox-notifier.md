---
title: "Mailbox Notifier"
description: "A DIY notifier when a new letter is delivered to a physical mailbox"
date: 2017-12-06T20:57:28.939Z
tags: [DIY]
---

Back in the day when there was no app that send you an announcement
for new letters and you had to check your physical mailbox daily
if you expect something important, I build this little gadget.

It was planned as a weekend project, so the schedule was quite tight.
After my first tests didn't work with a usual WiFi-based microcontroller
I got myself a `Particle Electron` to able to access the air-based interwebs.

![components](/images/mailbox-notifier/components.jpg)

![test](/images/mailbox-notifier/test.jpg)

![working notification](/images/mailbox-notifier/notification-email.jpg)

![installation](/images/mailbox-notifier/installation.jpg)


It somewhat worked when testing manually a few times, but I quickly
disassembled it a few days later, as the glowing mailbox and the visible cables
and components might've led the postman to call the police on me.
