---
title: Live debugging of microcontroller setups
description: 
date: 2019-12-30T18:06:50.233Z
tags: [DIY, ESP32]
---

Yesterday I killed my ESP32 due to my awesome live debugging skills of my Hydroponics setup. I've accidentally hit one GPIO pin with a 12 V power source.

Note to myself: Stop fiddling around with cables when the power is on. This advice might come in handy when handling PC components as well ...
