---
title: "@ E-Mail Notifier"
description: "Project for a hardware-based incoming mail notificator"
date: 2019-12-30T18:18:28.939Z
tags: [DIY]
---

At last I found some time and motivation for my next little project. The **@ E-Mail Notifier**!

The idea is to use a **Go** program to check a Gmail inbox for new mail regularly. The result will be sent to an **ESP8266** acting as interface between a PC and the relais controlling the @-lamp.

The Arduino code for the ESP is pretty simple and straightforward.

```c
/* 
 * This sketch will enable and disable a specific pin by input via the serial interface.
 * Send a "0" to deactivate and a "1" to activate.
 */

#define ACTIVATOR_PIN D6

// Initial state is off
int active = 0;

void setup() {
  pinMode(ACTIVATOR_PIN, OUTPUT);

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  if (Serial.available() > 0) {
    // get incoming byte:
    int inByte = Serial.read();
    if (inByte == 48) {
      active = 0;
      Serial.println("OFF");
    }
    else if (inByte == 49) {
      active = 1;
      Serial.println("ON");
    }
  }

  digitalWrite(ACTIVATOR_PIN, active);
  delay(100);
}
```

And the Gmail library for Go is also nice and easy to grasp (if you're used to the Google APIs and the authentication voodoo).


{{< youtube CZETXMbZNyE >}}

With an ESP at hand I could've done it completely embedded, but I wanted a practical use case for getting some experience in Golang.

Afterwards I 3D printed a basic case for the control unit and the relais, so nobody dies when touching it accidentally.

![Example image](/images/at-email-notifier-done.jpg)
