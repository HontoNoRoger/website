---
title: "Magic Mirror"
description: "A try at an intelligent mirror"
date: 2017-06-19T12:54:35.939Z
tags: [DIY]
---

First iteration of my magic mirror is up and running.
Mirror, mirror on the wall, tell me who is the most bored of them all?

![magic-mirror](/images/magic-mirror.jpg)
