---
title: Bitchy SD Cards
description: 
date: 2019-12-30T18:19:48.921Z
tags: 
---

Today I had a problem where I've flashed a new Raspberry Pi Image on my SD card. While this worked and I updated the system on the RPi, my screen tuned black and not back on again, so I rebooted the Pi (the hard way).

After countless hours of trying to fix the update, I noticed that none of the changes persisted. So I retrieved the SD card and tried to reflash it ... without success. Even with `dd` it wasn't possible to add or edit any data. But `dd` didn't scream at me either. Of course, I checked the lock slider at the SD card (multiple times even) but it was unlocked. Long story short for some reason it was broken so that reading worked but writing didn't. I scraped it and used another one without any issues. Weird problem which generated a lot of wasted time.
