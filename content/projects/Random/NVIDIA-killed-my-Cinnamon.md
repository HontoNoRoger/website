---
title: NVIDIA killed my Cinnamon
description: 
date: 2021-02-05T19:52:49.051Z
tags: [PC, Linux]
---

**Update:**

Bumblebee is not necessary anymore on recent Fedora versions.
I followed the [NVIDIA Guide from RPM Fusion](https://rpmfusion.org/Howto/NVIDIA) and I could achieve even faster frame rates that way. The only drawback is a not so memorable prefix for starting Steam games (or other applications).

So the primusrun command is now exchanged with:
`__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia %command%`
Which is a tad longer and more ugly, but it's working faster.
And of course there are always ways like aliases or wrappers to make it cleaner.

Another information I didn't provide before is that NVIDIA acceleration and Wayland don't work together nicely. So for no using X11 is unfortunately still the way to go.

**Original content:**

Some random package upgrades got my Cinnamon login on Ubuntu 18.04 getting stuck. I tried literally _everything_ I could find in the interwebs but nothing helped. Fortunately "Ubuntu on Wayland" session was still working, so I could do last backups and tasks.

Afterwards I decided to install Fedora with KDE spin since I decided to get faster but still stable updates and Gnome feels really old nowadays. After I set up everything I cried a little at the frame rate when starting Steam games and Minecraft, so I went for setting up the NVIDIA graphics drivers again. And again, I wasted hours to get it working. Wasn't it quite easy on Ubuntu previously? Then I learned about NVIDIA Optimus. The name itself oozes irony. Even Linus Torvalds only had a middle finger to spare for their semi-cool Linux implementation. Optimus is for seamless switching between an onboard Intel graphics adapter and a dedicated NVIDIA graphics card. On the default setup, the **nouveau** drivers do a pretty good job but the frame rate for 3D applications is sad. In the end, I only got the NVIDIA drivers working with **bumblebee** and have to start every game with `optirun -b none` prefix. Yes, `optirun -b primus` or `primusrun` doesn't give me the same framerate as none does. This is only because `primusrun`, which is recommended to use, properly limit frame rate to the notebook screen AFAICT. For Steam, I had to change the launch options for every game to `primusrun %command%`. With Steam providing **Proton** to use most Windows games under Linux I thought about getting rid of Windows completely, but NVIDIA did their good part to defend Windows _raison d’être_.

I'm not really sure if I'll stay on KDE, since it feels pretty bloated, but Fedora's package management with `dnf` is great. And I hate `snap`.
