---
title: SSH Signal Symphony
description: 
date: 2019-12-30T18:24:36.570Z
tags: [Administration, Linux]
---

Another day, another script. While doing some load tests on 8 different servers, I wanted them to run in parallel but being able to actually cancel the script. So I used my favorite [Terminator](https://terminator-gtk3.readthedocs.io/en/latest/index.html) to run those commands in parallel on the target machines with the format `ssh user@targetmachine awesomeloadtestcommand`. Sounds easy, right? Well, it wasn't.

When the Bash scripts I used to run the Python load tests were doing something I didn't intend them to, I tried `Ctrl + c` to terminate them early so I could fix the issues I implemented. Unfortunately, rather than my scripts terminating, it was the SSH session which got killed without any further notice. The Bash scripts I initially started were still running.

This is a totally different behaviour than what you are used to when using interactive SSH sessions. I got it [solved thanks to the omniscient RTFM StackOverflow](https://unix.stackexchange.com/questions/102061/ctrl-c-handling-in-ssh-session) (or rather the whole StackExchange platform). With the command format of `ssh -t user@targetmachine command` you force to allocate a TTY which also handles signals properly. This was great for debugging while also keeping the commands running when the SSH connection was lost when omitting the `-t` flag.

In 5+ years of using Linux professionally and personally I didn't have the need to know about the `-t` flag. But now that I know it I'll definitely incorporate it into my toolbox.
