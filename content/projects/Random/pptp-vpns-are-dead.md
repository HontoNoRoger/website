---
title: PPTP VPNs are dead
description: Trouble with PPTP VPNs and DS Lite ISP connections
date: 2020-03-19T14:07:35.654Z
tags: [Networking]
---

Just had the issue that a very simple PPTP VPN connection didn't want to connect.
The reason was that our ISP 1&1 only offers IPv4 via a DS Lite tunnel. So we basically
use a native IPv6 connection with tunneled IPv4.
Of course this makes sense in the age of IoT and companies offering PPTP only VPNs
are either not interested in home office or nekromancers.

Anyway, DS Lite (IPv4 via IPv6 tunnel) and PPTP doesn't work well together.