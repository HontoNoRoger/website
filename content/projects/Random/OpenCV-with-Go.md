---
title: OpenCV with Go
description: 
date: 2020-05-18T17:03:51.051Z
tags: [Machine Learning, OpenCV, Golang]
---

Getting started
---------------

I stumbled upon a great [OpenCV binding for Golang](https://gocv.io)
since I wanted to get my fingers on face detection for some
unspecified private project.

Building the libs wasn't as straight forward on my Fedora 31 laptop as the [docs](https://gocv.io/getting-started/linux/) suggested.

Before the compilation with `make install` happened successfully I had to add 
`PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig` to my environment variables permanently.
Additionally I've created a file `/etc/ld.so.conf.d/opencv-64.conf` and wrote:
`/usr/local/lib64/` in it.

Else both compiling OpenVINO (part of OpenCV) and running
openCV itself always complaining that my opencv libraries or
its dependencies cannot be found.


First quick project
-------------------

To get started I found this quick and easy tutorial for face detection and face recognition by [packagemain](https://pliutau.com/) (that dude has some great Go tutorials on his blog and YouTube channel):

{{< youtube rbZeZNVA-Q4 >}}

(Code examples at https://github.com/plutov/packagemain/tree/master/05-gocv-machinebox)

To test the face recognition part myself I needed a [MachineBox](https://machinebox.io) account and API key which are both free, but the company behind it advertises that everything is running in the Docker image, no processing on their side, keeping the pictures private (hopefully).

For the little amount of work I put in (seriously just following the 20 minute tutorial) it works really well. It only took one image of me to recognize my face in multiple ways.

For higher requirements on detection and recognizing quality it is not well suited or needs more work, i.e. I couldn't upload and/or train on images for recognition if they weren't capured frontal or with a weird face expression. It always rejected my images with "no face detected". The CascadeClassifier of OpenCV was able to detect a face in this situation though.

If I continue to rely more on Facebox I'd need to take a look into how I can optimize not only recognizing portrait images.
