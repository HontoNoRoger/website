---
title: Google Drive directory migration to Team Drives
description: 
date: 2019-12-30T18:01:28.527Z
tags: [Google, Drive, G Suite]
---

One thing I need to remind myself often again is that moving whole directory structures from Google Drive over to G Suite's "Team Drives" can be cumbersome. Don't get me wrong, using Team Drives is great as it simplifies access and sharing to a more comprehensible model. Due to normal Google Drive's graph nature for linking directories and files (instead of strictly hierarchical like in Team Drives or other "normal" file systems) it can result in people not finding their files when migrating to Team Drives.

Consider the following hierarchy existing in Google Drive:

```
Folder1
  - Folder2
    - File1
    - File2
  - Folder3
    - File2
```

When migrating _Folder1_ as is, you might expect to find _File2_ in subfolder 2 and 3, which is not the case. The migration process will transform the graph into a tree, so a file (or directory) can only have one parent. The first location the migration process finds will be used for the file. If it is iterated by a sorted ID index, I don't know. So the resulting location could be different than what you might've expected by name.
